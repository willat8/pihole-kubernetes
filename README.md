Ensure an NFS provisioner is the default storage class

    helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
    helm repo update
    helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --set nfs.server=192.168.0.10 --set nfs.path=/mnt/mythtv/pvs --set storageClass.defaultClass=true --set storageClass.reclaimPolicy=Retain

Run with

    helm repo add mojo2600 https://mojo2600.github.io/pihole-kubernetes/
    helm repo update
    helm install pihole mojo2600/pihole --set persistentVolumeClaim.enabled=true --set serviceWeb.loadBalancerIP=192.168.1.99 --set serviceWeb.annotations."metallb\.universe\.tf/allow-shared-ip"="pihole-svc" --set serviceWeb.type=LoadBalancer --set serviceDns.loadBalancerIP=192.168.1.99 --set serviceDns.annotations."metallb\.universe\.tf/allow-shared-ip"="pihole-svc" --set serviceDns.type=LoadBalancer --set DNS1=192.168.0.1 --set DNS2= --set extraEnvVars.ServerIP=192.168.1.99 --set 'regex={(\\.|^)androidtvwatsonfe-pa\\.googleapis\\.com$,(\\.|^)androidtvlauncherxfe-pa\\.googleapis\\.com$}'
